import * as authentication from '@feathersjs/authentication';
// Don't remove this comment. It's needed to format import lines nicely.
import { HookContext } from '@feathersjs/feathers';

const { authenticate } = authentication.hooks;

export default {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};

const addUserToLearningModule = async (
  context: HookContext
): Promise<HookContext> => {
  context.data.createdAt = new Date();
  const params = {
    query: { free: true },
  };

  const data = { userId: context.result._id, progress: '0' };

  const freeModules = await context.app.service('learning-modules').find(params);

  await freeModules.data.map((item: any) => {
    context.app
      .service('learning-modules')
      .patch(item._id, { $push: { users: data } });
  });

  return context;
};
