import app from '../../src/app';

describe('\'learning-modules\' service', () => {
  it('registered the service', () => {
    const service = app.service('learning-modules');
    expect(service).toBeTruthy();
  });
});
